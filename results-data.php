<div class="container">


    <div id="app">

        <table class="data-linked-Box">
            <thead>
            <tr class="table-header">
                <th>Draw ID</th>
                <th>Draw Date &amp; Time</th>
                <th>Winning Numbers</th>
            </tr>
            <tr>
            </thead>
            <tbody>

            <tr v-for="(item, index) in get_rows()" :key="index">
                <td>{{item.id}}</td>
                <td>{{item.date}}</td>
                <td>{{item.winningNumbers}}</td>
<!--                <td><a :href="`winners-detail-page.php?id=${item.id}`" class="" >see details</a></td>-->
            </tr>
            </tbody>

        </table> <div class="pagination">
            <div class="page-num">
                    <span style="cursor: pointer" class="page-link" v-for="i in num_pages()"
                          :class="[i == currentPage ? 'active' : '']"
                          @click="change_page(i)">{{i}}</span>
            </div>
        </div>

        <!--        <div class="pagination"><span class="pg-prev"><a href="#"><i class="fa fa-arrow-left"></i><em>Previous</em></a></span>-->
        <!--            <div class="page-num">-->
        <!--                <last id="first-last"><a href="#">First</a></last>-->
        <!--                <a href="#" class="page-link">1</a>-->
        <!--                <a href="#" class="page-link">2</a>-->
        <!--                <a href="#" class="page-link active">3</a>-->
        <!--                <a href="#" class="page-link">4</a>-->
        <!--                <a href="#" class="page-link">5</a>-->
        <!--                <last id="first-last"><a href="#">Last</a></last>-->
        <!--            </div>-->
        <!--            <span class="pg-next">-->
        <!--<a href="#"><em>Next</em><i class="fa fa-arrow-right"></i></a></span></div>-->
    </div>
</div>
<script type="module">
    import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.esm.browser.js'

    let app = new Vue({
        mounted() {
            self=this;
            axios.get('https://e3232e80-61f5-4343-af88-37cbd07428e3.mock.pstmn.io/results').then((response)=>{
                let responseData=(JSON.parse(JSON.stringify(response)))
                console.log(responseData.data)
                console.log(responseData.data.message)
                self.tableData=responseData.data.pickThreeDraws
            })
        },
        el: '#app',
        data() {
            return {
                hello: "hello",
                tableData: [],
                currentPage: 1,
                elementsPerPage: 11,
                ascending: false,
                sortColumn: '',

            }
        },
        methods: {
            getWinnersData() {
                console.log("hello")
            },
            "get_rows": function get_rows() {
                var start = (this.currentPage - 1) * this.elementsPerPage;
                var end = start + this.elementsPerPage;
                return this.tableData.slice(start, end);
            },
            "num_pages": function num_pages() {
                return Math.ceil(this.tableData.length / this.elementsPerPage);
            },
            "change_page": function change_page(page) {
                this.currentPage = page;
            }
        },
        computed:{

        }
    })
</script>
