<div class="box" id="box">
  <form method="post">
    <a class="boxclose" id="boxclose"></a>
    <h1>Hub Communications Client Login</h1>
    <div class="form-data-info">
      <div>
        <input name="login-email" type="text" placeholder="Enter your email / member number" />
      </div>
      <div>
        <input name="login-password" type="password" placeholder="Password" />
      </div>
      <div>
        <button name="login-button">Enter</button>
      </div>
      <!-- div class="forget-password"><a href="#">Forgot your password?</a> <a href="#">Create an account</a></div --> 
    </div>
  </form>
</div>
