<?php
if(isset($_POST["subscribe"])){
	$sql = "INSERT INTO subscriptions SET email='".mysqli_escape_string($con, $_POST["subscribe"])."'";
	$rst = mysqli_query($con, $sql);// or die(mysqli_error($con));
	$id = mysqli_insert_id($con);
	if($id){
		$notification = "<div class=\"notification\">You have successfully subscribed to Open Mind Africa</div>";
	}else{
		$notification = "<div class=\"notification\">Subscription failed or already exist, try again</div>";
	}
}
?>
<?=$notification?>
<div class="footer-inner">
<div class="col"><div class="footer-newsletter"><h3><?=CMS::render('oma_footer_content_1', CMS::TEXT, 'Stay Up to Date', $editmode)?></h3>
<p><?=CMS::render('oma_footer_content_2', CMS::TEXT, 'Subscribe to OMA mailing list to get the updates to your email box.', $editmode)?></p>
<form method="post">
	<div class="subscribe"><input type="text" name="subscribe" placeholder="Enter your email" /><button class="btn-sbt"><em class="fa fa-arrow-right"></em></button></div>
</form>

<div class="footer-contacts">
<?php $default = "Chaina: +86 186 0076 6028
Ghana: +233 244 402 565
info@openmindafrica.org"; ?>
<?=CMS::render('oma_footer_content_3', CMS::MULTI, $default, $editmode)?>
</div>
</div>
<div class="footer-content">
<div class="footer-link">
<h3 class="menu-title">Navigation</h3>

<div class="left-nav"><ul>
		<li><a href="about-oma.php">About Us</a></li>
		<li><a href="about-us.php">Vision Mission Strategy</a></li>
		<li><a href="oma-members.php">Students Profile</a></li>
		<li><a href="summer-camp2018.php">Summer Camp 2018</a></li>
                <li><a href="summer-camp.php">Summer Camp 2017</a></li>
		<li><a href="resources.php">Downloads</a></li>
		<li><a href="team.php">Directors</a></li>
		<li><a href="our-programs.php">Students Comment</a></li>
		<li><a href="faqs.php">FAQ's</a></li>
		<li><a href="oma-club.php">Campus Meeting</a></li>
		<li><a href="future-child.php">Future Child</a></li>
    				
		
		
</ul>
</div>
<div class="righ-nav"><ul>
		<li><a href="mentoring.php">Mentoring</a></li>
		<li><a href="guidance-and-counselling.php">Guidance & Counselling</a></li>
		<li><a href="scholarship.php">Cultural Exchange</a></li>
		<li><a href="benefits-students.php">SEL Benefits to Students</a></li>
		<li><a href="professional-development.php">Educator Summit</a></li>				
		<li><a href="benefits-schools.php">Beneficiary Schools</a></li>
		
		
		<li><a href="america-office.php">American Office</a></li>
		<li><a href="chinese-office.php">Chinese Office</a></li>
		<li><a href="contacts.php">Contact Us</a></li>
                <li><a href="curriculum.php">Curriculum</a></li>
<li><a href="terms-conditions.php">Terms and conditions</a></li>	
		
		<!--<li><a href="privacy.php">Privacy and Refund Policy</a></li>-->
		</ul></div>

</div>

<div class="social">
<h3 class="menu-title">Connect with OMA</h3>
<?=CMS::render('oma_social_content_1', CMS::MULTI, '<a href="https://www.facebook.com/openmindafrica/" target="_blank"><span class="facebook social-icon">facebook</span></a>', $editmode)?>
<?=CMS::render('oma_social_content_2', CMS::MULTI, '<a href="https://twitter.com/openmindafrica/" target="_blank"><span class="twitter social-icon">twitter</span></a>', $editmode)?>
<?=CMS::render('oma_social_content_3', CMS::MULTI, '<a href="https://www.instagram.com/openmindafrica/" target="_blank"><span class="instagram social-icon">twitter</span></a>', $editmode)?>
<?=CMS::render('oma_social_content_4', CMS::MULTI, '<a href="https://www.youtube.com/channel/UC1qcktxLjuDLp_LvCiPPcUQ/videos" target="_blank"><span class="youtube social-icon">youtube</span></a>', $editmode)?>
</div>
</div>
<div class="copyright">
<span class="creation-year">Copyright © <?php
$date=new DateTime(); //this returns the current date time
$result = $date->format('Y');
echo $result;
?>  OMA. All rights reserved.</span>
<span class="designby">design+code: <a href="http://newsarena.com.gh" target="_blank">Hub Communications</a></span>
</div>
</div>


</div>
<script type="text/javascript">
	$(function(){
		function hideNotification(){
			$(".notification").fadeOut(100);
		}
		setTimeout(hideNotification, 7000);
	});
</script>


<script>
$('#accordion').find('.accordion-toggle').click(function () {

		//Expand or collapse this panel
		$(this).next().slideToggle('fast');

		//changes arrow 
		if ($(this).find('.arrow').hasClass('arrowUp')) {
				$(this).find('.arrow').removeClass('arrowUp').addClass('arrowDown');
		} else {
				$('#accordion').find('.arrow').removeClass('arrowUp').addClass('arrowDown');
				$(this).find('.arrow').removeClass('arrowDown').addClass('arrowUp');
		}
		//Hide the other panels
		$(".accordion-content").not($(this).next()).slideUp('fast');

});
//# sourceURL=pen.js
</script>