
  <div class="wrapper"><p class="hero-mbl"><?=CMS::render('lk_mobile_header_content', CMS::TEXT, 'Pick your 3 lucky numbers', $editmode)?></p>
  
  <div class="apps-icon">
                                          <a href="#" title="Download Android App"><img src="production/images/googlePlay.png" alt="Download Android App" title="Download Android App"/></a>
                                          <a href="#" title="Download IOS App"><img src="production/images/AppStore.png" alt="Download IOS App" title="Download IOS App"/></a></div>
    <!--<a href="javascript:avoid(0)" class="modal-toggle click-play-btn">Click to Play Now</a>-->
  </div>
  
  <div class="modal">
    <div class="modal-overlay modal-toggle"></div>
    <div class="modal-wrapper modal-transition">
      <div class="modal-header">
        <button class="modal-close modal-toggle"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
<g>
	<path class="p-close" d="M26,0C11.7,0,0,11.7,0,26s11.7,26,26,26s26-11.7,26-26S40.3,0,26,0z M26,50C12.8,50,2,39.2,2,26S12.8,2,26,2
		s24,10.8,24,24S39.2,50,26,50z"/>
	<path class="p-close" d="M35.7,16.3c-0.4-0.4-1-0.4-1.4,0L26,24.6l-8.3-8.3c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4l8.3,8.3
		l-8.3,8.3c-0.4,0.4-0.4,1,0,1.4c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3l8.3-8.3l8.3,8.3c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3
		c0.4-0.4,0.4-1,0-1.4L27.4,26l8.3-8.3C36.1,17.3,36.1,16.7,35.7,16.3z"/>
</g>
</svg></button>
      </div>
      
      <div class="modal-body">
        <div class="modal-content">
      
      
      
      <div class="bet-app animated bounceIn">
                    <form>
                        <div>
                        <label class="check-reset"><img src="production/images/mnos-mtn.png">
                            <input type="radio" checked="checked" name="radio" id="">
                            <span class="checkmark"></span> </label>
                        <label class="check-reset"><img src="production/images/mnos-airtel.png">
                            <input type="radio" name="radio" id="">
                            <span class="checkmark"></span> </label>
                        <label class="check-reset"><img src="production/images/mnos-voda.png">
                            <input type="radio" name="radio" id="">
                            <span class="checkmark"></span> </label>
                      </div>
                        <div>
                        <input type="tel" x-autocompletetype="tel" pattern="\d*" placeholder="Mobile Phone Number" id="">
                        <label class="app-l telephone_digit">+233</label>
                      </div>
                        <div>
                        <h5>What are your 3 Lucky Numbers?</h5>
                        <input type="number" pattern="\d*" placeholder="0" id="">
                        <input type="number" pattern="\d*" placeholder="5" id="">
                        <input type="number" pattern="\d*" placeholder="9" id="">
                      </div>
                        <div>
                        <h5>Enter Your Stake (from GHC 2)</h5>
                        <label class="app-l">GHC</label>
                        <input name="amount" type="number" required class="deposit-amount" placeholder="2" oninvalid="this.setCustomValidity('Please enter your desire Amount')" oninput="this.setCustomValidity('')" id="">
                      </div>
                        <div>
                        <input type="submit" value="Play Now" class="submit_btn fa fa-arrows">
                      </div>
                      </form>
                  </div>

        </div>
      </div>
    </div>
  </div>

<script>
// Quick & dirty toggle to demonstrate modal toggle behavior
$('.modal-toggle').on('click', function(e) {
  e.preventDefault();
  $('.modal').toggleClass('is-visible');
});
</script>

