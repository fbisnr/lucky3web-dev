<div class="promo-app"> <span id='close-promo'><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 83.2 83.2" style="enable-background:new 0 0 83.2 83.2;" xml:space="preserve">
<circle class="close-ads" cx="41.6" cy="41.6" r="41.6"/>
<g>
	<path d="M55.6,27.4c-0.5-0.5-1.4-0.5-1.9,0L42.5,38.7L31.3,27.4c-0.5-0.5-1.4-0.5-1.9,0s-0.5,1.4,0,1.9l11.2,11.3L29.4,51.9
		c-0.5,0.5-0.5,1.4,0,1.9c0.3,0.3,0.6,0.4,1,0.4c0.3,0,0.7-0.1,1-0.4l11.2-11.3l11.2,11.3c0.3,0.3,0.6,0.4,1,0.4
		c0.3,0,0.7-0.1,1-0.4c0.5-0.5,0.5-1.4,0-1.9L44.4,40.6l11.2-11.3C56.1,28.8,56.1,27.9,55.6,27.4z"/>
</g>
</svg>
</span><div> 
Dear valued player, we have refunded your cash plus 20%. Sorry for the inconvenience caused in playing NLA Mid-Week. All Games close at 6pm on the platform.</div></div>
