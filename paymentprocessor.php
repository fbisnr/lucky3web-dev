<?php
include('config/payment-connect.php');

$params = [
    'format' => 'json',
    'msisdn' =>  isset($_POST['msisdn'])?$_POST['msisdn']:'',
    'amount'   => isset($_POST['amount'])?$_POST['amount']:'',
    'numbers'=> isset($_POST['numbers'])?$_POST['numbers']:'',
    'operator'=> isset($_POST['operator'])?$_POST['operator']:'',
    'accessChannel'=>'WEB_MOBILE'
];

// Send the POST request with cURL
$ch = curl_init($url);
$payload = json_encode($params);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($payload)
));

// Process your response here
$result = curl_exec($ch);
curl_close($ch);
$response=(array)json_decode($result);
error_reporting( error_reporting() & ~E_NOTICE );

echo json_encode(['code'=>200, 'msg'=>$response]);