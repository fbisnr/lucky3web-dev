
<div class="nav-container" id="mobile-disable_nav">

  
  <div class="navbar-header">
    <div class="toggle-button" id="toggle">
      <span class="bar top"></span>
      <span class="bar middle"></span>
      <span class="bar bottom"></span>
    </div>
  </div>

<nav class="overlay" id="overlay">


  <ul>
    <li><a href="index.php">home</a></li>
    <li><a href="results.php">Results</a></li>
<!--    <li><a href="winners.php">Winners</a></li>-->
    <li><a href="htp.php">How to play</a></li>
    <li><a href="tcs.php">T&amp;Cs</a></li>
    <li><a href="contact.php">Contacts</a></li>
  </ul>
  <!--<div class="winners-mobile-ads"><h1 class="show-header">Congratulations our JackPot Winner!</h1>
    <img src="production/images/winner-poster.jpg" width="500" height="234"  alt="Winner Poster" title="Winner Poste"/></div>-->
</nav>
  </div>
  
  
  

<script>
$("#toggle").click(function() {
  //on click, the hamburger menu elements get the toggle-active class and the overlay gets the nav-active class
  $(this).toggleClass("toggle-active");
  $("#overlay").toggleClass("nav-active");

  //the h1 element toggles between visibility and invisibility
  $("h1").toggleClass("hidden");
});
</script>