


<link rel="stylesheet" href="production/css/responsiveslides.css" type="text/css">
<link rel="stylesheet" href="production/css/demo.css?v=5" type="text/css">
<link href="production/css/reset.css?v=3" rel="stylesheet" type="text/css">

<link href="production/css/web-ui.css?v=16" rel="stylesheet" type="text/css">

<link href="production/css/animate.css" rel="stylesheet" type="text/css">
<link href="production/css/mobile-nav.css" rel="stylesheet" type="text/css">
<link href="production/css/clock.css" rel="stylesheet" type="text/css">
<link href="production/css/w3-animate.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="production/css/cms-ui.css?v=2">


<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
<link rel="manifest" href="site.webmanifest">
<link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="theme-color" content="#ffffff">

<meta name="msapplication-TileColor" content="#00aba9">
<meta name="theme-color" content="#ffffff">

<script async data-id="16760" src="https://cdn.widgetwhats.com/script.min.js"></script>

<script type="text/javascript" src="production/js/jquery.min.js"></script>
<script type="text/javascript" src="production/js/wow.min.js"></script>
<script src="production/js/responsiveslides.min.js"></script>
<script src="production/js/jquery.mobilePhoneNumber.js"></script>

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
jQuery(function($){
      var input = $('[type=tel]')
      input.mobilePhoneNumber({allowPhoneWithoutPrefix: '+233'});
      input.bind('country.mobilePhoneNumber', function(e, country) {
        $('.country').text(country || '')
      })
	});
});
</script>


 <!--Homepage Hero Images-->
 
<script>
    // You can also use "$(window).load(function() {"
    $(function () {

      // Slideshow 1
      $("#slider1").responsiveSlides({
        maxwidth: 800,
        speed: 800
      });

      // Slideshow 2
      $("#slider2").responsiveSlides({
        auto: false,
        pager: true,
        speed: 300,
        maxwidth: 540
      });

      // Slideshow 3
      $("#slider3").responsiveSlides({
        manualControls: '#slider3-pager',
        maxwidth: 540
      });

      // Slideshow 4
      $("#slider4").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        before: function () {
          $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
          $('.events').append("<li>after event fired.</li>");
        }
      });

    });
  </script>
  
<script>
function openCity(cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  document.getElementById(cityName).style.display = "block";  
}
</script>


 <!--Networks Operators Switcher-->

<script>

            window.onload = function () {
                startTab();
            };

            function startTab() {
                document.getElementById("defaultOpen").click();

            }

            function openProcess(evt, processName) {
                // Declare all variables
                var i, tabcontent, tablinks;

                // Get all elements with class="tabcontent" and hide them
                tabcontent = document.getElementsByClassName("tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }

                // Get all elements with class="tablinks" and remove the class "active"
                tablinks = document.getElementsByClassName("tablinks");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                }

                // Show the current tab, and add an "active" class to the link that opened the tab
                document.getElementById(processName).style.display = "block";
                evt.currentTarget.className += " active";
				//evt.currentTarget.className += " airtel";
            }
        </script>
        
        
 <!--Scroll To Top-->
 <script>
 $('html, body').animate({
    scrollTop: $("#target-element").offset().top
}, 1000);
 </script>

