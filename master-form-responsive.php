<form id="responsiveForm">
    <div>
        <label class="check-reset"><img src="production/images/mnos-mtn.png">
            <input type="radio"  name="operator" value="mtn" checked>
            <span class="checkmark"></span> </label>
        <label class="check-reset"><img src="production/images/mnos-airtel.png">
            <input type="radio" name="operator" value="airtel">
            <span class="checkmark"></span> </label>
        <label class="check-reset"><img src="production/images/mnos-voda.png">
            <input type="radio" name="operator" value="voda">
            <span class="checkmark"></span> </label>
    </div>
    <div>
        <input type="tel" x-autocompletetype="tel" pattern="\d*" placeholder="Mobile Phone Number" id="" name="mobile" required maxlength="9">
        <label class="app-l telephone_digit">+233</label>
<!--        <div class="country"></div>-->
    </div>
    <div>
        <h5>What are your 3 Lucky Numbers?</h5>
        <input type="number" pattern="\d*" placeholder="0" id="" name="lucky_1" required min="0" max="9">
        <input type="number" pattern="\d*" placeholder="0" id="" name="lucky_2" required min="0" max="9">
        <input type="number" pattern="\d*" placeholder="0" id="" name="lucky_3" required min="0" max="9">
    </div>
    <div>
        <h5>Enter Your Stake (from GHC2)</h5>
        <label class="app-l">GHC</label>
        <input name="amount" type="number" required class="deposit-amount" placeholder="2-5,000" oninvalid="this.setCustomValidity('Please enter your desire Amount')" oninput="this.setCustomValidity('')" id="" min="2" max="5000">
    </div>
    <div>
        <input type="submit" value="Play Now" class="submit_btn fa fa-arrows" id="submit1">
    </div>
    <div id="payment-notification-responsive" class="success-notification">
        Approve the payment in the USSD message
    </div>
    <div id="payment-notification-error-responsive" class="error-notification">
        Payment Unsuccessful
    </div>
</form>
<script type="text/javascript">
    $("#payment-notification-responsive").hide();
    $("#payment-notification-error-responsive").hide();
    $(document).ready(function(e){
        $("#submit1").click(function(e){
            var formStatus = $('#responsiveForm').validate({
                rules: {
                    mobile: {
                        required: true
                    },
                    lucky_1: {
                        required: true
                    },lucky_2: {
                        required: true
                    },lucky_3: {
                        required: true
                    },
                    amount:{
                        required:true
                    }
                },
                messages: {
                    mobile: "enter the correct mobile number",
                    lucky_1: "required",
                    lucky_2: "required",
                    lucky_3: "required",
                    amount:"enter the desired amount"
                },
            }).form();
            if (formStatus==true){
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "paymentprocessor.php",
                    data: {
                        format:"json",
                        msisdn: $("#operator").val(),
                        amount:$("#amount").val(),
                        numbers:$("#lucky_1").val()+","+$("#lucky_2").val()+","+$("#lucky_3").val(),
                        operator:$("#operator").val(),
                        accessChannel:"WEB_MOBILE"
                    },
                    beforeSend: function(){
                        // Show image container
                        $("#submit1").val("Playing ......");

                    },
                    success : function(data){
                        let response =JSON.parse(data.replace(/(<([^>]+)>)/ig,""));
                        console.log(response)
                        $("#submit1").val("Play Now");
                        if (response["msg"]['status']==="-1"){
                            clearFields();
                            $("#payment-notification-error-responsive").show().delay(6000).fadeOut();

                        }else {
                            clearFields();
                            $("#payment-notification-responsive").show().delay(6000).fadeOut();
                        }
                    }
                });
            }
        })

    });

    function clearFields(){
        $("input[name=mobile]").val('');
        $("input[name=lucky_1]").val('');
        $("input[name=lucky_2]").val('');
        $("input[name=lucky_3]").val('');
        $("input[name=amount]").val('');
        $("input[name=radio]").attr('checked', 'checked');
    }
</script>