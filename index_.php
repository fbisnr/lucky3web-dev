
<?php
function active($currect_page){
  $url_array =  explode('/', $_SERVER['REQUEST_URI']) ;
  $url = end($url_array);  
  if($currect_page == $url){
      echo 'active'; //class name in css 
  } 
}
?>

<!doctype html>
<html>
  <head>
  <meta charset="UTF-8">
  <meta name="description" content="Trends, Celebs, Gossip, Lifestyle">
  <meta name="keywords" content="Keed,Keed-NLA,Keed NLA,Keed Ghana,Ghana,West Africa,Africa,Lottery,Lotto,Lotteries,Lucky 3,Lucky3,Lucky three,Keed Lottery,KeedGhana,Ghana lotto,Ghana lottery,Lottery Ghana,Ghana lotteries,Lotteries Ghana,Keed Lucky 3,Lucky 3 Keed,Keed NLA Lottery,Lottery Keed NLA,Keed Lottery,Lottery Keed,Keed Lucky3,Lucky3 Keed,Keed jackpot,Jackpot,Winning,Winnings,Wins,Winner">
  <meta name="author" content="Keed-NLA">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Keed-NLA Lucky3 - Numbers Game</title>
<?php include('scripts.php') ?>
  </head>

  <body>
<section> 
    <!--Site Header Begins-->
    <section>
    <header class="_site-header" id="site-header-app">
        <div class="grid grid-pad">
        <div class="col-1-1">
            <div class="content">
             <?php /*?><?php include('mobile-nav.php'); ?><?php */?>
            <?php include('nav.php'); ?>
            
            <!--Application-->
            <div class="Application-Clicks">
            <!--<h2 class="home-hero-mbl">Pick your 3 Lucky
                    Number &amp; Win Big</h2>-->
            <?php include('pop.php'); ?>
            </div>
            
            <div class="home-app">
                <div class="col-1-1">
                <div class="col-1-3">
                    <div class="bet-app animated bounceIn">
                    <form>
                        <div>
                        <label class="check-reset"><img src="production/images/mnos-mtn.png">
                            <input type="radio" checked="checked" name="radio" id="">
                            <span class="checkmark"></span> </label>
                        <label class="check-reset"><img src="production/images/mnos-airtel.png">
                            <input type="radio" name="radio" id="">
                            <span class="checkmark"></span> </label>
                        <label class="check-reset"><img src="production/images/mnos-voda.png">
                            <input type="radio" name="radio" id="">
                            <span class="checkmark"></span> </label>
                      </div>
                        <div>
                        <input type="tel" x-autocompletetype="tel" pattern="\d*" placeholder="Mobile Phone Number" id="">
                        <label class="app-l telephone_digit">+233</label>
                        <div class="country"></div>
                      </div>
                        <div>
                        <h5>What are your 3 Lucky Numbers?</h5>
                        <input type="number" pattern="\d*" placeholder="0" id="">
                        <input type="number" pattern="\d*" placeholder="0" id="">
                        <input type="number" pattern="\d*" placeholder="0" id="">
                      </div>
                        <div>
                        <h5>Your Stake (from GHC2)</h5>
                        <label class="app-l">GHC</label>
                        <input name="amount" type="number" required class="deposit-amount" placeholder="2-5,000" oninvalid="this.setCustomValidity('Please enter your desire Amount')" oninput="this.setCustomValidity('')" id="">
                      </div>
                        <div>
                        <input type="submit" value="Play Now" class="submit_btn fa fa-arrows">
                      </div>
                      </form>
                  </div>
                  </div>
                <div class="col-1-3 home-summary">
                    <h2 class="ac-gn-link-hero animated bounce">Pick your 3 Lucky
                    Number &amp; Win Big</h2>
                    <p>Lucky3 is the first of it's kind lucky numbers Game in Ghana and gives MTN, AirtelTigo and Vodafone mobile money users a chance to win big.</p>
                    <p>All you need to do is choose 3 numbers from 0 to 9 to win up to 300x your money every 10 minutes.</p>
                    <div class="go-app"><a href="htp.php" class="go-linked"><em class="go"></em>Learn More</a></div>
                  </div>
              </div>
              </div>
          </div>
          </div>
      </div>
      </header>
  </section>
    <!--Ends-->
    <section>
    <div class="callbacks_container">
        <ul class="rslides" id="slider4">
        <li> <img src="production/images/homepage-hero.jpg" alt=""> </li>
      </ul>
      </div>
  </section>
    <section id="body-app">
    <section class="home-intro-hero">
        <div class="grid grid-pad">
        <div class="col-1-1">
            <div class="content">
            <div class="dial-me-app animated bounceIn">
                <div class="col-1-2">
                <div class="win-week-div">
                    <h2>win up to</h2>
                    <h1>500,000 in jackpot</h1>
                    <h3>every week</h3>
                  </div>
              </div>
                <div class="col-1-2">
                <div class="dial-987">
                    <h5><em>just dial</em>
                    <p>*987#</p>
                  </h5>
                  </div>
              </div>
              </div>
            <div class="hero-big animated swing">
            <div id="deposit"><?php include('play-with-mnos.php'); ?></div>
                <div class="money-hero"><em>Time is</em> <img src="production/images/money.png"></div>
                <div class="intro-summary">
                <ul class="home-bullets"><li>Lucky 3 is the latest numbers game from the NLA where you win cash every 10 minutes</li>
<li>You can win up to 300x your stake when you match 3 numbers in the correct order</li>
<li>Win up to a maximum guaranteed cash amount of GHc 1,500,000 every 10 minutes</li>
<li>Get extra cash on the side in the Daily Jackpots made specially for you to win more</li>
<li>Win up to GHc 500,000 cash in addition during the live TV Jackpot draw every week</li>

<li>Play anytime, anywhere and on any type of phone. No internet connection needed</li>
<li>No registration required, play now instantly from your mobile money account</li>
<li>Play Lucky 3 every 10 minutes, 24 hours every day, 52 weeks in a year</li>
<li>All winnings are paid out automatically and directly into your mobile money account </li>
<li>Lucky 3 is the only game in Ghana with the Best Odds and Fastest payouts.</li></ul>
</div>
              </div>
          </div>
          </div>
      </div>
      </section>
    <section class="section-three-app">
        <div class="grid grid-pad">
        <div class="col-1-1">
            <div class="content">
            <div class="nla-content">
                <p>What are your 3 Lucky Numbers?</p>
                <span class="select-3-numbers"><img src="production/images/3-numbers.png" alt=""></span> </div>
          </div>
          </div>
      </div>
      </section>
    <section class="home-intro-hero">
        <div class="grid grid-pad">
        <div class="col-1-1">
            <div class="content">
            <div class="jackpot">
            <div class="money-hero"><img src="production/images/jackpot-hero.png"></div>
            <p>Every thursday at 9:30PM the Lucky 3 Jackpot is live on TV3! <br>
              This and every Thursday you can win up to GHC 500,000.00 Cash!!!<br>
              Every GHC 2.00 you spend this week qualifies you for 1 free entry in the Lucky 3.</p>
              <!--<img class="prize" src="production/images/prize.png" alt="Prize" title="Prize"/>-->
<!--Count Down Clock begins here--><!--<div><h1>Countdown Clock</h1>
<div id="clockdiv">
  <div>
    <span class="days"></span>
    <div class="smalltext">Days</div>
  </div>
  <div>
    <span class="hours"></span>
    <div class="smalltext">Hours</div>
  </div>
  <div>
    <span class="minutes"></span>
    <div class="smalltext">Minutes</div>
  </div>
  <div>
    <span class="seconds"></span>
    <div class="smalltext">Seconds</div>
  </div>
</div>
</div>-->

	<!--<div><h1>Countdown Clock</h1>
<div id="clockdiv">
  <div>
    <span class="days"></span>
    <div class="smalltext">Days</div>
  </div>
  <div>
    <span class="hours"></span>
    <div class="smalltext">Hours</div>
  </div>
  <div>
    <span class="minutes"></span>
    <div class="smalltext">Minutes</div>
  </div>
  <div>
    <span class="seconds"></span>
    <div class="smalltext">Seconds</div>
  </div>
</div>
</div>-->
	



<?php /*?><?php
$weekday = 4;
if((gmdate("N") == $weekday && gmdate("H") >= 5 && gmdate("H") < 22)){
  if(gmdate("N") == $weekday){
    $deadline = (strtotime('this thursday 10pm') - strtotime(gmdate("Y-m-d H:i:s"))) * 1000;
  }else{
    $deadline = (strtotime('next thursday 10pm') - strtotime(gmdate("Y-m-d H:i:s"))) * 1000;
  }
?><?php */?>


<div class="jackpot-show-app">
<fieldset><legend><h1>The Jackpot Show</h1></legend>

<div class="grid grid-pad"><div class="content">
<div class="col-1-3"><div class="video-thumbnail"><iframe width="300" height="150" src="https://www.youtube.com/embed/SAxSwyo4WJY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<p>Caption goes here</p></div>
</div>
<div class="col-1-3"><div class="video-thumbnail"><iframe width="300" height="150" src="https://www.youtube.com/embed/n2KEJ3kG9oY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><p>Caption goes here</p></div></div>

<div class="col-1-3"><div class="video-thumbnail"><iframe width="300" height="150" src="https://www.youtube.com/embed/DPIeJzIVXG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><p>Caption goes here</p></div></div></div></div>

</fieldset>

</div>
            </div>
            </div>
          </div>
      </div>
      </section>
  </section>
    <footer>
    <div>
        <?php include('footer.php'); ?>
      </div>
  </footer>
    <div class="app-side-bar">
    <ul>
        <li><a href="#" title="Download Mobile App"><span></span></a></li>
      </ul>
  </div>
  </section>
<script src="production/js/mobileValidate.js"></script> 
<script src="production/js/jquery.mobilePhoneNumber.js"></script> 

<!--Start of Tawk.to Script--> 
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d9d78ea6c1dde20ed05b30b/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script> 


  <?php /*?><script type="text/javascript">
  function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime) {
  var clock = document.getElementById(id);
  var daysSpan = clock.querySelector('.days');
  var hoursSpan = clock.querySelector('.hours');
  var minutesSpan = clock.querySelector('.minutes');
  var secondsSpan = clock.querySelector('.seconds');

  function updateClock() {
    var t = getTimeRemaining(endtime);

    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}



var deadline = new Date(Date.parse(new Date()) + <?=$deadline?>);
initializeClock('clockdiv', deadline);
  </script>
  <?php 
}
?><?php */?>
<!--End of Tawk.to Script-->

</body>
</html>