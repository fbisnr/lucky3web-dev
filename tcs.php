<?php
function active($currect_page){
  $url_array =  explode('/', $_SERVER['REQUEST_URI']) ;
  $url = end($url_array);  
  if($currect_page == $url){
      echo 'active'; //class name in css 
  } 
}
?>
<!doctype html>
<html>
  <head>
  <meta charset="UTF-8">
  <meta name="description" content="Trends, Celebs, Gossip, Lifestyle">
  <meta name="keywords" content="Keed,Keed-NLA,Keed NLA,Keed Ghana,Ghana,West Africa,Africa,Lottery,Lotto,Lotteries,Lucky 3,Lucky3,Lucky three,Keed Lottery,KeedGhana,Ghana lotto,Ghana lottery,Lottery Ghana,Ghana lotteries,Lotteries Ghana,Keed Lucky 3,Lucky 3 Keed,Keed NLA Lottery,Lottery Keed NLA,Keed Lottery,Lottery Keed,Keed Lucky3,Lucky3 Keed,Keed jackpot,Jackpot,Winning,Winnings,Wins,Winner">
  <meta name="author" content="Keed-NLA">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Keed-NLA Lucky3 - Numbers Game</title>
<?php include('scripts.php') ?>
  </head>

<body>
<section>

<!--Site Header Begins-->
<section>
<header class="_site-header" id="site-header-app">
              <div class="grid grid-pad">
              <div class="col-1-1">
              <div class="content">
                <?php include('nav.php'); ?>
                
              </div></div></div>
            </header>
</section>
<!--Ends-->
<section>
<div class="callbacks_container">
        <ul class="rslides" id="slider4">
          <li> <img src="production/images/tcs-page-banner.jpg" alt=""> </li>
        </ul><span class="no-show-app"><?php include('play-display-app.php'); ?></span>
      </div></section>
      

<section id="body-app">

<section class="site-wrapper-app"><div class="grid grid-pad">

<div class="col-1-1 bottom-play-app no-show-app no-show-app-desktop"><?php include('play-display-app.php'); ?></div>

<div class="col-9-12 data-app"><div class="content">
<div class="page-section-flu _no_padding"><div class="conact-app page-content">
  <h4 class="header-text">Terms &amp; Conditions</h4>
   <p><strong>Definitions</strong></p>
   <p><strong>Bet / Stake:</strong> The mechanism used by the player to enter the game.</p>
   <p><strong>Draw:</strong> The Promoter uses this process which results in the random selection of a set of Winning numbers for a game.</p>
   <p><strong>Draw Procedures:</strong> The draw procedures that apply to a Draw, as determined by the Promoter from time to time.</p>
   <p><strong>Game:</strong> An event or outcome which is determined by a Draw.</p>
   <p><strong>Mobile Money:</strong> Mobile Wallets of MTN and AirtelTigo and Vodafone</p>
   <p><strong>Prize:</strong> A Prize of cash or including non-cash Prizes;</p>
   <p><strong>Ticket Number:</strong> The unique number in the ticket which identifies your bet / stake, will be recorded on the Promoters computer system.</p>
   <p><strong>Winning Numbers:</strong> The numbers that are drawn from the Random Number Generator (RNG).</p>
   <p><strong>The Promoter</strong>: The promoter is Keed Ghana Ltd operating as KEED-NLA and is a registered company in accordance with the laws of Ghana under the Companies Act, 1963 (Act 179) with registration number CS239962107 whose registration address at North Labone.</p>
   <p>Keed Ghana Limited (Ltd) is authorized and regulated by the National Lottery Authority.</p>
   <p> <strong>The Competition</strong>: The title of the competition is Lucky 3</p>
   <p> <strong>How to Enter</strong></p>
   <p>3.1. The competition will run on USSD code *987#, SMS AND online.</p>
   <p>3.2. To enter the competition online, simply choose the network you have a mobile money account with, enter your mobile number, type your 3 lucky numbers and submit the amount of money you want to bet with, wait for mobile money pin request &amp; approve to confirm entry into the 10 min draw.</p>
   <p>3.3. To enter the competition via SMS, simply send a FREE SMS of your 3 lucky numbers from 0 to 9, leave a space, enter amount and send to 987. If you do not enter 3 numbers correctly e.g. you input only one number or two, then the system randomly generates a 3-number code.</p>
   <p>Any other character input will automatically generate a random 3-number code.</p>
   <p>Users can also enter a full stop (.) to have a random selection.</p>
   <p>3.4. Once you accept terms and conditions and dial *987#, you become a player and get the chance to win;</p>
   <ol start="1" type="1">
     <li>a) Up to 300 times your money if you pick 3 Winning numbers correctly in the correct order;</li>
     <li>b) 6 times your money if you pick 3 Winning numbers in any order;</li>
     <li>c) 2.5 times your money if you pick 2 Winning numbers in any order;</li>
     <li>d) Get your money back if you match any one number in the same order as the numbers in the draw (i.e. 1 of your selected numbers matches same position as the draw number - in which event your bet amount will be returned to you)</li>
   </ol>
   <p>3.5. The winning numbers for each draw will be generated on a random basis. These winning numbers are randomly generated every 10 minutes by a Certified Random Number Generator (RNG) engine. Upon entry into a draw, players get a confirmation by text of their selections and the Draw they are in.</p>
   <p>3.6. The Promoter will not accept responsibility for competition entries that are, mislaid, damaged or delayed in transit, regardless of cause, including, for example, as a result of any equipment failure, technical malfunction, systems, satellite, network, server, computer hardware or software failure of any kind.</p>
   <p>3.7. By submitting a competition entry, you are agreeing to be bound by these terms and conditions.</p>
   <p>3.8. For help with entries, please contact the Consumer Helpline *987 on all networks OR visit keed-nla.com</p>
   <p>3.9. Please visit website keed-nla.com for a copy of these competition terms and conditions.</p>
   <p>3.10. The stake for each Lucky 3 Draw is between GHC 2.00 minimum to GHC 5,000.00 maximum.</p>
   <p>3.11. The results can be checked by visiting keed-nla.com. The results will also be sent to all players that made a Bet/Stake in the game via SMS.</p>
   <p>3.12. The draw is done every 10 minutes of every day (including weekends) – However the time, frequency, date and method of the draws for each game shall be decided by the promoter.</p>
   <p>3.13. In the event there is an equipment failure or failure of any other sort or for another reason a draw is not completed and therefore cannot take place on the date and time fixed, it may take place as soon as practically possible after that date.</p>
   <p>3.14. In the event any draw is deemed invalid, a further draw will take place in line with the draw procedures to decide the Winning Numbers.</p>
   <p>3.15. A Prize will be paid by the Promoter based only on official results of any relevant draw recorded in the promoter&rsquo;s computer system. In the event a prize is notified in the newspapers or on any websites or other medium, the promoter shall not pay the prize.</p>
   <p>3.16. The promoter with its discretion can occasionally introduce other play to win prize promotions under the Lucky 3 game that enable players an opportunity to play and win additional prize money aside the regular Lucky 3 game. All details of such promotional activity in effect shall be clearly stipulated in clauses below:</p>
   <p>3.16.i. Weekly Jackpot:</p>
   <ul type="disc">
     <li>Every week all Lucky 3 tickets played will be entered into a pool and fed into a random number generator (RNG).</li>
     <li>The NLA will oversee and certify the RNG for each weekly draw and one or more tickets will be selected as the Lucky player(s) for the week.</li>
     <li>KEED-NLA will then call to screen winners (i.e. Age, Valid ID, Phone Line ownership, etc.), on successful validation, winner(s) will be invited to join the live show in TV3 studios in Accra the next week.</li>
     <li>During the show, winner(s) will each be granted a seed cash amount to play with live on the show. (NB: Seed cash amount to be determined solely by promoter at each point in time)</li>
     <li>Winner(s) will reserve the right to cash out seed money without spinning the wheel of fortune or play with all or part of seed cash on the wheel of fortune for a chance to either:</li>
     <ul type="circle">
       <li>Win more money for landing on either of the varied cash multipliers on the wheel</li>
       <li>Risk losing the seed cash when the spin dial lands on any of the zero (0) multipliers</li>
       <li>Chance to enter and spin the inner wheel when the outer spin dial lands on &ldquo;Draw&rdquo;</li>
       <li>Win the ultimate jackpot of up to GHC 500,000 (jackpot cash amount for each week will vary on sole discretion of promoter)</li>
       <li>All cash amounts won will be validated and processed for collection at the KEED-NLA office after the draw.</li>
       <li>The weekly jackpot show will be produced and aired live on TV3 each Thursday at 9.30pm and uploaded on the KEED-NLA Facebook page.</li>
     </ul>
   </ul>
   <p> 3.16.ii. Daily Jackpot:</p>
   <ul type="disc">
     <li>Every day excluding (i.e. Thursday Weekly Jackpot Days) there will be a daily jackpot that rewards 2 Lucky players with GHc 1,000 cash</li>
     <li>Eligibility is reserved to all players who meet the legal gambling age of 18 years and above and have played a minimum of 1 ticket within the period</li>
     <li>Each day, 2 winners will be selected to win the GHc 1,000 cash prize:</li>
     <ul type="circle">
       <li>Highest Player: Any player who played the most tickets (count) each day before the draw</li>
       <li>Random Player: Any player (new or existing) who&rsquo;s ticket is selected by the random number generator (RNG) from the pool of tickets played before the draw</li>
       <li>Daily jackpot period will be 23 hours and 59 secs from 6pm each day (excluding Thursdays for weekly jackpot). Draws will be held daily at 6pm which will consider the pool of tickets from the previous 23 hours 59 secs (i.e. from 6pm the previous day)</li>
     </ul>
   </ul>
   <p>All winnings shall be processed via mobile money and announced across KEED-NLA social media handles and other selected traditional media channels</p>
   <p><strong>4. Eligibility</strong></p>
   <p>4.1. The competition is only open to all residents in Ghana aged 18 years and over except;</p>
   <p>(a) Employees of the Promoter or its holding or subsidiary companies.</p>
   <p>(b) Employees of agents or suppliers of the Promoter or its holding or subsidiary companies, who are professionally connected with the competition or its administration.</p>
   <p>(c) Members of the immediate families or households of (a) and (b) above.</p>
   <p>4.2. In entering the competition, you confirm that you are eligible to do so and eligible to claim any prize you may win. The Promoter may require you to provide proof that you are eligible to enter the competition.</p>
   <p>4.3. The Promoter will not accept competition entries that are;</p>
   <p>(a) Automatically generated by computer.</p>
   <p>(b) Completed by third parties or in bulk.</p>
   <p>4.4. The Promoter reserves all rights to disqualify you if your conduct is contrary to the spirit or intention of the prize competition.</p>
   <p> 5.1. The prizes are as follows;</p>
   <ol start="1" type="1">
     <li>a) Up to 300 times your money if you pick 3 Winning numbers correctly in the correct order;</li>
     <li>b) 6 times your money if you pick 3 Winning numbers in any order;</li>
     <li>e) 2.5 times your money if you pick 2 Winning numbers in any order;</li>
     <li>d) Get your money back if you match any one number in the same order as the numbers in the draw (i.e. 1 of your selected numbers matches same position as the draw number - in which event your bet amount will be returned to you)</li>
   </ol>
   <p>5.2. The Prize is not negotiable or transferable.</p>
   <p>5.3. After each draw and announcement of winners, their prize money shall be paid directly into their account unless the Promoter at their discretion decides to present any given prize directly to the winner.</p>
   <p>5.4. Prizes that are over GHC 50,000 will not be paid via mobile wallets. Winners will be contacted so that they can be presented with their prize.</p>
   <p>6. <strong>Winner Announcement</strong></p>
   <p>6.1. The winner of the competition will be announced every 10 minutes. Each draw will close after 10 minutes and restarts for the next draw. If you play after a 10-minute duration (i.e. your bet is not registered on system for a particular 10 minute draw) you will automatically go into the next draw.</p>
   <p>6.2. The decision of the Promoter is final, and no correspondence or discussion will be entered into.</p>
   <p>6.3. Winners will have their money sent directly to their mobile wallet after the draw if they win any amount up to GHC 50,000.</p>
   <p>6.4. Winners who win more than GHC 50,000 will have to collect their winnings and will be contacted by SMS and any other legitimate communication channel by Lucky 3 with instructions on how to redeem.</p>
   <p>6.5. The Promoter reserves the right to contact the winner personally as soon as practicable after the win, using the telephone number used or email address or any other contact number provided in the competition entry. The Promoter will not amend any contact information once the competition entry has been submitted.</p>
   <p>6.6. A list of Winners can be viewed on website keed-nla.com for a period of 14 days (every 2 weeks). The Promoter will endeavour to ensure that the results (including Prize Breakdowns) and winning prizes displayed on the Website or published via other sources are accurate.</p>
   <p>Although every effort is made to ensure the accuracy of this information, the promoter shall not be liable for any mistakes in respect of such information.</p>
   <p>6.7. If required by law, or by any court, or by any regulations, the promoter may provide your details to a third party if you win a prize and full details of any prize paid to you.</p>
   <p>6.8. From time to time the promoter may send you messages of a promotional nature through any contact details provided to the promoter upon registration until you opt out of this service.</p>
   <p>6.9. The Promoter may be entitled at any time to withdraw or suspend or close any Game at any time at its absolute discretion.</p>
   <p> 7. <strong>Claiming the prize</strong></p>
   <p>7.1. In the event the Promoter decides to provide the prize to a winner face to face at the</p>
   <p>Promoter&rsquo;s office the following provisions apply;</p>
   <p>7.2. The Prize shall not be claimed by a third party on your behalf.</p>
   <p>7.3. The Promoter will make all reasonable efforts to contact the winner. If the winner cannot be contacted or is not available or has not claimed their prize within 14 days of the win, the promoter reserves the right to withdraw the prize.</p>
   <p>7.4. The promoter does not accept any responsibility if you are not able to take up the Prize.</p>
   <p>7.5. You are not entitled to claim a Prize by solely using any slips or any bet confirmation provided by the Promoter.</p>
   <p>7.6. Each Prize must be claimed before the end of the day after the Draw (&ldquo;the Claim Period&rdquo;) using the claim procedure as set out in clause 7.7 below and if the procedure in clause 7.7 is not used to claim the Prize within the Claim Period, then you shall lose the prize and the prize will not be paid by the Promoter.</p>
   <p>7.7. A Prize can be claimed in any one of the following ways: -</p>
   <ol start="1" type="1">
     <li>a) By contacting the Consumer Helpline (*987) on a 24-hour daily basis which is fully displayed on the website;</li>
     <li>b) By visiting the offices of the Promoter during the promoter&rsquo;s normal office hours.</li>
   </ol>
   <p>7.8. If you have not claimed your prize within the Claim Period, you can for a further period of</p>
   <p>14 working days after the Claim Period contact the Promoter in the following ways and providing all the information the promoter requires so that the Promoter may consider your request;</p>
   <ol start="1" type="1">
     <li>a) Phoning the consumer Helpline on *987 during its normal opening hours from time to time.</li>
     <li>b) Emailing <a href="mailto:customerservice@keed-nla.com">customerservice@keed-nla.com</a></li>
     <li>c) If you fail to claim the Prize within the additional 5-day period, your entitlement to the Prize will be lost and the prize will not be paid.</li>
   </ol>
   <p>7.9. Any Prize prior to payment shall be validated by the Promoter in line with its procedures as notified on the website from time to time and the Promoters decision shall be final and binding on you.</p>
   <p>7.10. A Bet shall be invalid if any of the following events occur: -</p>
   <ol start="1" type="1">
     <li>a) The Bet is placed to increase the chance of any person winning a Prize, or to increase the prize for that Draw.</li>
     <li>b) A Prize is not claimed within the Claim Period under clause 7.7</li>
     <li>c) The Bet is not in the list published on the Promoters website.</li>
     <li>d) The Bet has in any way been forged.</li>
     <li>e) The Bet fails the Promoters validation check.</li>
     <li>f) The Promoter cannot match the ticket number to any records held by the promoter on its computer systems.</li>
     <li>g) The Promoter at its discretion is concerned about the validity of the Bet.</li>
     <li>h) The Promoter believes that the Prize is being claimed by someone who is not the account holder.</li>
     <li>i) The Bet has been tampered with.</li>
     <li>j) The Bet has not been issued or sold by the Promoter.</li>
   </ol>
   <p>7.11. The following rights are always reserved to the Promoter at its sole discretion: -</p>
   <ol start="1" type="1">
     <li>a) Make payment of any Prize by any means including, by bank transfer, direct credit, Mobile Money or by cheque.</li>
     <li>b) Until such time as a Bet has been validated withhold the Prize until all validation has been completed to the Promoter&rsquo;s satisfaction.</li>
     <li>c) Request you to attend its offices to claim the Prize;</li>
     <li>d) Undertake any relevant validation or security checks at its discretion without any recourse to you and invalidate any Bets as a result of any problems with the Bet;</li>
     <li>e) Request proof of identification and entitlement to claim the Prize;</li>
     <li>f) Refuse to pay a Prize.</li>
     <li>g) Keep records of anybody claiming a Prize including photographic evidence.</li>
     <li>h) Refer the matter to a court or government agency to consider whether a prize should be paid;</li>
     <li>i) Refuse to pay a Prize if you refuse to sign all the documentation required by the</li>
   </ol>
   <p><strong>Promoter</strong></p>
   <p>7.12. In the event that a Prize has already been claimed fraudulently by the third party the promoter shall not be obliged to repay the Prize to you.</p>
   <p>7.13. In the event that the Promoter withholds payment of the Prize for whatever reason you shall not be entitled to any interest or compensation whilst matters are being resolved and the Prize remains unpaid.</p>
   <p>7.14. You are responsible for the payment of any charges levied by the mobile operator payment gateways with respect to cashing out prize money.</p>
   <p> 8. <strong>Limitation of liability</strong></p>
   <p>8.1. Insofar as is permitted by law, the Promoter, its agents or distributors will not in any circumstances be responsible or liable to compensate the winner or accept any liability for any loss, damage, personal injury or death occurring as a result of taking up the prize except where it is caused by the negligence of the Promoter, its agents or distributors or that of their employees. Your statutory rights are not affected.</p>
   <p>8.2. In the event that there is a dispute arising from any non-payment or payment of Prizes the Promoters decision shall be final and saved for the Promoter may at its sole discretion reimburse the cost of the Bet to you.</p>
   <p>8.3. The Promoters liability to you shall be limited to the amount of the Bet.</p>
   <p>8.4. The Promoter may withhold payment of a Prize until any dispute has been resolved.</p>
   <p> 9. <strong>Ownership of competition entries and intellectual property rights</strong></p>
   <p>9.1. All competition entries and any accompanying material submitted to the Promoter will become the property of the promoter on receipt and will not be returned.</p>
   <p>9.2. You agree that the Promoter may, but is not required to, make your entry available on its website keed-nla.com and any other media, whether now known or invented in the future, and in connection with any publicity of the competition. You agree to grant the promoter a non-exclusive, worldwide, irrevocable license, for the full period of any intellectual property rights in the competition entry and any accompanying materials, to use, display, publish, transmit, copy, edit, alter, store, re-format and sub-license the competition entry and any accompanying materials for such purposes.</p>
   <p> 10.<strong> Data protection and publicity</strong></p>
   <p>10.1. If you are the winner of the competition, you agree that the Promoter may use your name, image and town or county of residence to announce the winner of this competition and for any other reasonable and related promotional purposes.</p>
   <p>10.2. You further agree to participate in any reasonable publicity required by the Promoter.</p>
   <p>10.3. By entering the competition, you agree that any personal information provided by you in the competition entry may be held and used only by the promoter or its agents and suppliers to administer the competition.</p>
   <p> 11.<strong> General</strong></p>
   <p>11.1. If there is any reason to believe that there has been a breach of these terms and conditions, the promoter may, at its sole discretion, reserve the right to exclude you from participating in the competition.</p>
   <p>11.2. The Promoter reserves the right to hold void, suspend, cancel, or amend the prize competition where it becomes necessary to do so.</p>
   <p>11.3. These terms and conditions shall be governed by Ghanaian law, and the parties submit to the non-exclusive jurisdiction of the courts of Ghana.</p>
   <p> </p> 
   
    <!--<div class="accordion_container">
  <div class="accordion_head" style="display:block;">First Accordian Head<span class="plusminus">+</span></div>
  <div class="accordion_body" style="display: block;">
    <p><strong>Bet / Stake:</strong> The mechanism used by the player to enter the game.</p>
    <p><strong>Draw:</strong> The Promoter uses this process which results in the random selection of a set of Winning numbers for a game.</p>
    <p><strong>Draw Procedures:</strong> The draw procedures that apply to a Draw, as determined by the Promoter from time to time.</p>
    <p><strong>Game:</strong> An event or outcome which is determined by a Draw.</p>
    <p><strong>Mobile Money:</strong> Mobile Wallets of MTN and AirtelTigo and Vodafone</p>
    <p><strong>Prize:</strong> A Prize of cash or including non-cash Prizes;</p>
    <p><strong>Ticket Number:</strong> The unique number in the ticket which identifies your bet / stake, will be recorded on the Promoters computer system.</p>
    <p><strong>Winning Numbers:</strong> The numbers that are drawn from the Random Number Generator (RNG).</p>
  </div>
  <div class="accordion_head">The Promoter<span class="plusminus">+</span></div>
  <div class="accordion_body" style="display: none;">
    <p>The promoter is Keed Ghana Ltd operating as KEED-NLA and is a registered company in accordance with the laws of Ghana under the Companies Act, 1963 (Act 179) with registration number CS239962107 whose registration address at North Labone.

Keed Ghana Limited (Ltd) is authorized and regulated by the National Lottery Authority.

</p>
  </div>
  <div class="accordion_head">Third Accordian Head<span class="plusminus">+</span></div>
  <div class="accordion_body" style="display: none;">
    <p>Third Accordian Body, it will have description</p>
  </div>
</div>-->
    
    
</div></div>
<div class="col-1-1 bottom-play-app no-show-app-desktop_"><?php include('play-display-app.php'); ?></div>

</div></div></div></section>
</section>

<footer>
<div>
      <?php include('footer.php'); ?>
      </div>
    </footer>

    
</section>
<script type="text/javascript">
$(document).ready(function() {
  //toggle the component with class accordion_body
  $(".accordion_head").click(function() {
    if ($('.accordion_body').is(':visible')) {
      $(".accordion_body").slideUp(300);
      $(".plusminus").text('+');
    }
    if ($(this).next(".accordion_body").is(':visible')) {
      $(this).next(".accordion_body").slideUp(300);
      $(this).children(".plusminus").text('+');
    } else {
      $(this).next(".accordion_body").slideDown(300);
      $(this).children(".plusminus").text('-');
    }
  });
});

</script>



<?php include('play-app.php'); ?>
</body>
</html>